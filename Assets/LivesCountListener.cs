﻿using UnityEngine;
using System.Collections;

public class LivesCountListener : MonoBehaviour {
	LifeSystem lifeSystem;
	[SerializeField] UnityEngine.UI.Text text;

	void OnAwake(){
		lifeSystem = GameObject.FindObjectOfType<LifeSystem>();
		text = GetComponent<UnityEngine.UI.Text>();
	}

	void OnEnable(){
		if(lifeSystem == null)
			lifeSystem = GameObject.FindObjectOfType<LifeSystem>();
		lifeSystem.OnLifeRecovered += OnLifeEarnedHandler;
		lifeSystem.OnLifeConsumed += OnLifeLostHandler;
	}

	void OnDisable(){
		lifeSystem.OnLifeRecovered -= OnLifeEarnedHandler;
		lifeSystem.OnLifeConsumed -= OnLifeLostHandler;
	}

	void OnLifeEarnedHandler(int newLives){
		text.text = newLives.ToString();
	}

	void OnLifeLostHandler(int newLives){
		text.text = newLives.ToString();
	}

}
