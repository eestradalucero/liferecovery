﻿using UnityEngine;
using System.Collections;

public class TimeToRecover : MonoBehaviour {

	LifeSystem lifeSystem;
	[SerializeField] UnityEngine.UI.Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<UnityEngine.UI.Text>();
		lifeSystem = GameObject.FindObjectOfType<LifeSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		text.text = Mathf.Abs((float)lifeSystem.timeTillNextRecovery.TotalMinutes).ToString("00.00");
	
	}
}
