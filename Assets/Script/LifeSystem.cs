﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class LifeSystem : MonoBehaviour {
	int maxLives = 5;
	int lives = 5;
	int timeToRecoverLife = 5;
	DateTime oldDate; 	
	DateTime currentDate;
	Queue<DateTime> lifeRecoveryQueue = new Queue<DateTime>();
	public TimeSpan timeTillNextRecovery { get { 
			return lifeRecoveryQueue.Count > 0 ? DateTime.Now - lifeRecoveryQueue.Peek() : TimeSpan.Zero;
			} }
	

	public delegate void LifeSystemEvent(int newLivesCount);
	public LifeSystemEvent OnLifeRecovered;
	public LifeSystemEvent OnLifeConsumed;

	void Update(){
		CheckRecovery();
		

		if(Input.GetKeyDown(KeyCode.A)){
			ConsumeLife();
		}
	}

	void CheckRecovery(){
		bool shouldCheckNext = true;
		DateTime now = DateTime.Now;
		while(shouldCheckNext && lifeRecoveryQueue.Count > 0){
			if(ShouldRecoverLife(lifeRecoveryQueue.Peek(), now)){
				lifeRecoveryQueue.Dequeue();
				RecoverLife();
				CallRecoveryDelegate();

			} else { 
				shouldCheckNext = false; 
			}
		}
	}

	bool ShouldRecoverLife(DateTime recoverTime, DateTime now){ 
		if(now > recoverTime){
			return true;
		}
		return false;
	}

	public void RecoverLife() {
		lives++;
		lives = Mathf.Clamp(lives, 0, maxLives);
		if(lives == maxLives && lifeRecoveryQueue.Count > 0){
			lifeRecoveryQueue.Clear();
		}
	}

	void CallRecoveryDelegate(){
		if(OnLifeRecovered != null){
			OnLifeRecovered(lives);
		}
	}

	void OnApplicationPause(bool pauseStatus) {
		if(pauseStatus){
			oldDate = DateTime.Now;
		} else {
			CheckRecovery();
		}
	}

	public void ConsumeLife(){
		if(lives > 0){
			lives--;
			if(lifeRecoveryQueue.Count == 0){
				lifeRecoveryQueue.Enqueue(DateTime.Now.AddSeconds(timeToRecoverLife));
			} else {
				int count = lifeRecoveryQueue.Count;
				var firstRecoveryDate = lifeRecoveryQueue.Peek();
				lifeRecoveryQueue.Enqueue(firstRecoveryDate.AddSeconds(timeToRecoverLife * count));
			}
			CallLifeLostDelegate();
			Debug.Log("Lives left: " + lives);
		}
	}

	void CallLifeLostDelegate(){
		if(OnLifeConsumed != null){
			OnLifeConsumed(lives);
		}
	}



}
